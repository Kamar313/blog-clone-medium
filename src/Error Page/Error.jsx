import React from "react";
import { NavLink } from "react-router-dom";

function Error() {
  return (
    <div>
      <NavLink to="/">
        <button className=" absolute bg-slate-400 top-2 left-3 px-2 rounded-lg shadow-lg">
          Go to Home
        </button>
      </NavLink>
      <img
        className=" w-full"
        src="https://cdn.mos.cms.futurecdn.net/PuXipAW3AXUzUJ4uYyxPKC-1200-80.jpg"
        alt="error"
      />
    </div>
  );
}

export default Error;
