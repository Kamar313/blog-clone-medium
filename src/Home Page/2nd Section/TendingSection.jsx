import React, { Component } from "react";
import { connect } from "react-redux";
import { dataFetch } from "../../Redux/Action & Reducer /action";
import Loader from "../../Loader/Loader";
import { NavLink } from "react-router-dom";
class TendingSection extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.dataFetch();
  }
  render() {
    return (
      <div className="w-1/1 min-h-[400px] flex justify-center pt-3">
        <div className=" w-5/6 pt-12">
          <h2 className=" font-semibold text-sm mb-4 mt-4">
            <img
              className=" inline w-5 mr-2"
              src="./photos/TrengingLogo.webp"
              rel="trenging logo"
            ></img>
            TRENDING ON MEDIUM
          </h2>
          <div className=" flex flex-wrap max-sm:justify-center ">
            {this.props.data.data.loading ? (
              <Loader />
            ) : (
              this.props.data.data.data.map((ele, i) => {
                return i < 6 ? (
                  <div
                    key={i}
                    className=" w-72 bg-slate-100 shadow-lg h-32 mr-12 max-sm:mr-0 mb-8 rounded-lg flex justify-center"
                  >
                    <div className=" w-1/6 h-full border-r-2 ">
                      <h3 className=" pl-1 text-4xl mt-2 font-semibold  text-gray-400 opacity-30">
                        {`0${i + 1}`}
                      </h3>
                    </div>
                    <div className=" w-5/6 pl-4">
                      <NavLink to={`articles/${ele.slug}`}>
                        <div className=" flex">
                          <img
                            className="rounded-full w-8 h-8 mb-1 mt-1 mr-1"
                            src={ele.author.image}
                            rel="Profile-photo"
                          />
                          <p className=" self-center text-[13px] font-semibold">
                            {ele.author.username}
                          </p>
                        </div>
                        <h2 className=" text-base font-semibold mr-2 mb-2 text-[16px] underline cursor-pointer ">
                          {`${ele.title.substring(0, 40)}`}
                        </h2>
                        <div className=" flex text-[12px]">
                          <span className=" mr-2">9 Jan</span>
                          <span className=" mr-2">5 Min Read</span>
                          <div className=" ">
                            <svg
                              width="16"
                              height="16"
                              viewBox="0 0 20 20"
                              fill="none"
                            >
                              <path
                                d="M12.4 12.77l-1.81 4.99a.63.63 0 0 1-1.18 0l-1.8-4.99a.63.63 0 0 0-.38-.37l-4.99-1.81a.62.62 0 0 1 0-1.18l4.99-1.8a.63.63 0 0 0 .37-.38l1.81-4.99a.63.63 0 0 1 1.18 0l1.8 4.99a.63.63 0 0 0 .38.37l4.99 1.81a.63.63 0 0 1 0 1.18l-4.99 1.8a.63.63 0 0 0-.37.38z"
                                fill="#FFC017"
                              ></path>
                            </svg>
                          </div>
                        </div>
                      </NavLink>
                    </div>
                  </div>
                ) : (
                  ""
                );
              })
            )}
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return { data: state };
}
export default connect(mapStateToProps, { dataFetch })(TendingSection);
