import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { dataFetch } from "../../Redux/Action & Reducer /action";
import { tagFetch } from "../../Redux/Action & Reducer /action";
import Loader from "../../Loader/Loader";
import { filterbyTag } from "../../Redux/Action & Reducer /action";

class GloblePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: false,
      checkUser: false,
      data: null,
    };
  }
  onClickPrivet = () => {
    this.setState({ checkUser: true });
  };
  onClickGloble = () => {
    this.setState({ checkUser: false });
  };
  datafeched = () => {
    this.props.dataFetch();
  };
  tagsFeched = () => {
    this.props.tagFetch();
  };
  tagfilter = (e) => {
    this.props.filterbyTag(e.target.innerHTML);
    this.setState({ filter: true });
  };
  fethAPIForYourFeed = () => {
    let athorize = JSON.parse(localStorage.getItem("data"));
    fetch("https://api.realworld.io/api/articles?limit=10&offset=0", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        authorization: `Token ${athorize.user.token}`,
      },
    })
      .then((ele) => ele.json())
      .then((data) => {
        let arr = [];
        data.articles.map((ele) => {
          if (ele.author.username == athorize.user.username) {
            arr.push(ele);
          }
        });
        this.setState({ data: arr });
      });
  };

  componentDidMount() {
    this.datafeched();
    this.tagsFeched();
    if (JSON.parse(localStorage.getItem("data"))) {
      this.fethAPIForYourFeed();
    }
  }

  render() {
    // console.log(this.props.data.data.tags.tags);
    console.log(this.props.data.data);
    console.log(this.props);
    return (
      <div className=" w-1/1 min-h-[350px] flex justify-center pt-12 pb-12">
        <div className=" w-5/6 flex justify-center">
          <div className=" w-[60%] max-sm:w-5/6 ">
            {JSON.parse(localStorage.getItem("data")) ? (
              <div>
                <h3
                  onClick={this.onClickGloble}
                  className={
                    this.state.checkUser
                      ? "cursor-pointer mr-4 font-medium inline-block h-8"
                      : " text-[#5cb85c] cursor-pointer mr-4 font-medium inline-block h-8 border-b-2 border-[#5cb85c]"
                  }
                >
                  Global Feeds
                </h3>
                <h3
                  onClick={this.onClickPrivet}
                  className={
                    this.state.checkUser
                      ? " text-[#5cb85c] cursor-pointer mr-4 font-medium inline-block h-8 border-b-2 border-[#5cb85c]"
                      : "cursor-pointer mr-4 font-medium inline-block h-8"
                  }
                >
                  Your Feeds
                </h3>
              </div>
            ) : (
              <h3 className=" text-[#5cb85c] font-medium inline-block h-8 border-b-2 border-[#5cb85c]">
                Globle Feeds
              </h3>
            )}

            <div className=" mt-2">
              {!this.props.data.data.loading ? (
                this.state.checkUser ? (
                  !this.state.data ? (
                    <Loader />
                  ) : (
                    this.state.data.map((ele) => {
                      return (
                        <div className=" mb-8 border-b-[1px] border-gray-300 border-opacity-80">
                          <div className=" flex justify-between">
                            <div className=" flex">
                              <img
                                className=" h-10 w-10 rounded-full"
                                src={ele.author.image}
                                rel="profile-image"
                              ></img>
                              <div className=" ml-2">
                                <h2 className=" hover:underline cursor-pointer">
                                  {ele.author.username}
                                </h2>
                                <p className=" font-normal text-xs text-gray-500">
                                  6 Jan 2019
                                </p>
                              </div>
                            </div>
                          </div>

                          <div className=" flex justify-between max-sm:flex-col max-sm:items-center">
                            <div className=" cursor-pointer pr-2 max-sm:order-3">
                              <NavLink to={`articles/${ele.slug}`}>
                                <h2 className=" text-base mt-2 max-sm:text-center">
                                  {ele.title}
                                </h2>
                                <p className=" text-slate-400 text-sm max-sm:text-center">
                                  {ele.description.substring(0, 120)}
                                </p>
                              </NavLink>
                            </div>
                            <img
                              src="./photos/blog-image-2.webp"
                              alt="blog-Photo"
                              className=" w-28 h-28 pb-2 max-sm:order-2 max-sm:mt-2 "
                            />
                          </div>

                          <div className=" flex justify-between cursor-pointer  max-sm:flex-col">
                            <div className=" flex mb-4">
                              {ele.tagList.map((ele) => {
                                return (
                                  <p className=" ml-2 px-2 border-[1px] text-xs text-gray-400 bg-gray-200">
                                    {ele}
                                  </p>
                                );
                              })}
                            </div>
                          </div>
                        </div>
                      );
                    })
                  )
                ) : this.state.filter ? (
                  this.props.data.data.filteredData.map((ele) => {
                    return (
                      <div className=" mb-8 border-b-[1px] border-gray-300 border-opacity-80">
                        <div className=" flex justify-between">
                          <div className=" flex">
                            <img
                              className=" h-10 w-10 rounded-full"
                              src={ele.author.image}
                              rel="profile-image"
                            ></img>
                            <div className=" ml-2">
                              <h2 className=" hover:underline cursor-pointer">
                                {ele.author.username}
                              </h2>
                              <p className=" font-normal text-xs text-gray-500">
                                6 Jan 2019
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className=" flex max-sm:flex-col max-sm:items-center">
                          <div className=" cursor-pointer pr-2 max-sm:order-3">
                            <NavLink to={`articles/${ele.slug}`}>
                              <h2 className=" text-base mt-2 max-sm:text-center">
                                {ele.title}
                              </h2>
                              <p className=" text-slate-400 text-sm max-sm:text-center">
                                {ele.description.substring(0, 120)}
                              </p>
                            </NavLink>
                          </div>
                          <img
                            src="./photos/blog-image-2.webp"
                            alt="blog-Photo"
                            className=" w-28 h-28 pb-2 max-sm:order-2 max-sm:mt-2 "
                          />
                        </div>

                        <div className=" flex justify-between cursor-pointer  max-sm:flex-col">
                          <div className=" flex mb-4">
                            {ele.tagList.map((ele) => {
                              return (
                                <p className=" ml-2 px-2 border-[1px] text-xs text-gray-400 bg-gray-200">
                                  {ele}
                                </p>
                              );
                            })}
                          </div>
                        </div>
                      </div>
                    );
                  })
                ) : (
                  this.props.data.data.data.map((ele, i) => {
                    return (
                      <div className=" mb-8 border-b-[1px] border-gray-300 border-opacity-80">
                        <div className=" flex justify-between">
                          <div className=" flex">
                            <img
                              className=" h-10 w-10 rounded-full"
                              src={ele.author.image}
                              rel="profile-image"
                            ></img>
                            <div className=" ml-2">
                              <h2 className=" hover:underline cursor-pointer">
                                {ele.author.username}
                              </h2>
                              <p className=" font-normal text-xs text-gray-500">
                                6 Jan 2019
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className=" flex max-sm:flex-col max-sm:items-center">
                          <div className=" cursor-pointer pr-2 max-sm:order-3">
                            <NavLink to={`articles/${ele.slug}`}>
                              <h2 className=" text-base mt-2 max-sm:text-center">
                                {ele.title}
                              </h2>
                              <p className=" text-slate-400 text-sm max-sm:text-center">
                                {ele.description.substring(0, 120)}
                              </p>
                            </NavLink>
                          </div>
                          <img
                            src="./photos/blog-image-2.webp"
                            alt="blog-Photo"
                            className=" w-28 h-28 pb-2 max-sm:order-2 max-sm:mt-2 "
                          />
                        </div>

                        <div className=" flex justify-between cursor-pointer  max-sm:flex-col">
                          <div className=" flex mb-4">
                            {ele.tagList.map((ele) => {
                              return (
                                <p className=" ml-2 px-2 border-[1px] text-xs text-gray-400 bg-gray-200">
                                  {ele}
                                </p>
                              );
                            })}
                          </div>
                        </div>
                      </div>
                    );
                  })
                )
              ) : (
                <Loader />
              )}
            </div>
          </div>
          <div className=" w-[40%] pl-2 max-sm:hidden">
            <h3 className=" mb-2 text-xl font-medium ">Popular Tags</h3>
            <div className=" ml-2 flex flex-wrap pr-10">
              {!this.props.data.data.loading
                ? this.props.data.data.tags.tags.map((ele) => {
                    return (
                      <button
                        onClick={this.tagfilter}
                        className=" bg-slate-100 px-2 border-[1px] text-sm font-light mr-2 mb-2"
                      >
                        {ele}
                      </button>
                    );
                  })
                : ""}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return { data: state };
}
export default connect(mapStateToProps, { dataFetch, tagFetch, filterbyTag })(
  GloblePost
);
