import React, { Component } from "react";
import Header from "../Top Bar/Header";
import Loader from "../../Loader/Loader";
import { NavLink } from "react-router-dom";

export default class SingleArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }
  fetchingData = () => {
    fetch(
      `https://api.realworld.io/api/articles/${this.props.match.params.slug}`
    )
      .then((res) => res.json())
      .then((data) => this.setState({ data: data }));
  };
  deleteBlog = () => {
    let athorize = JSON.parse(localStorage.getItem("data"));
    fetch(
      `https://api.realworld.io/api/articles/${this.props.match.params.slug}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          authorization: `Token ${athorize.user.token}`,
        },
      }
    );
    this.props.history.push("/");
  };
  componentDidMount() {
    this.fetchingData();
  }
  render() {
    let athorize = JSON.parse(localStorage.getItem("data"));
    return (
      <>
        <Header />
        <div className=" pt-20 max-sm:pt-14">
          {this.state.data == null ? (
            <Loader />
          ) : (
            <div>
              <div className=" h-44 max-sm:min-h-0 max-sm:mb-6 flex justify-center">
                <div className=" w-4/6 mt-2 max-sm:w-5/6 ">
                  <h1 className=" text-4xl mb-6 max-sm:text-2xl max-sm:mb-4">
                    {this.state.data.article.title}
                  </h1>
                  <div className=" flex justify-between">
                    <div className=" flex">
                      <img
                        src={this.state.data.article.author.image}
                        alt="profile-photo"
                        className=" rounded-full w-10 h-10 mr-1"
                      />
                      <div>
                        <p className=" text-sm">
                          {this.state.data.article.author.username}
                        </p>
                        <p className=" text-[12px]">6-jan-2019</p>
                      </div>
                    </div>
                    {JSON.parse(localStorage.getItem("data")) ? (
                      this.state.data.article.author.username ==
                      athorize.user.username ? (
                        <div>
                          <button
                            onClick={this.deleteBlog}
                            className=" mr-2 px-4 py-1 bg-slate-500"
                          >
                            Delete
                          </button>
                        </div>
                      ) : (
                        ""
                      )
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
              <div className=" flex justify-center">
                <img src="https://media.istockphoto.com/id/887987150/photo/blogging-woman-reading-blog.jpg?s=612x612&w=0&k=20&c=7SScR_Y4n7U3k5kBviYm3VwEmW4vmbngDUa0we429GA="></img>
              </div>
              <div className="  flex justify-center border-b-2">
                <div className=" w-4/6 mt-6 pb-6 max-sm:w-5/6 max-sm:text-sm ">
                  <p>{this.state.data.article.body.split("\\n").join(" ")}</p>
                </div>
              </div>
              <div className=" mt-10 flex justify-center">
                {JSON.parse(localStorage.getItem("data")) ? (
                  <div className=" w-5/6 px-10 mb-10">
                    <textarea
                      placeholder="Add Your Comment"
                      name="for Comment"
                      className=" mb-4 placeholder:italic max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
                      cols="30"
                      rows="8"
                    ></textarea>
                    <button className=" p-4 bg-slate-400 shadow-lg rounded">
                      Post Comment
                    </button>
                  </div>
                ) : (
                  <p className=" mb-10">
                    <NavLink to="/signin">
                      <span className=" text-orange-500 cursor-pointer underline">
                        Sign In
                      </span>
                    </NavLink>{" "}
                    or{" "}
                    <NavLink to="/signup">
                      <span className="text-orange-500 cursor-pointer underline">
                        Sign Up
                      </span>
                    </NavLink>{" "}
                    To Comment on this post.
                  </p>
                )}
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}
