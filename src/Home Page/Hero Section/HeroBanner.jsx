import React from "react";
import { NavLink } from "react-router-dom";
function HeroBanner() {
  return (
    <>{JSON.parse(localStorage.getItem("data")) ? "" : <NonAthorized />}</>
  );
}

function NonAthorized() {
  return (
    <div className="w-1/1 min-h-[550px] pt-20 max-sm:min-h-[300px] max-sm:pt-10 bg-[#FFC017] flex justify-center border-b-[1px] border-black">
      <div className=" w-5/6 h-1/6 flex max-sm:justify-center">
        <div className=" w-[55%] max-sm:w-[100%] max-sm:text-center ">
          <h1 className=" text-8xl font-normal max-sm:text-4xl mt-14">
            Stay curious.
          </h1>
          <h4 className=" text-2xl pr-20 mt-4 max-sm:pr-0 max-sm:text-base">
            Discover stories, thinking, and expertise from writers on any topic.
          </h4>
          <NavLink to="/signin">
            <button className=" text-xl h-10 bg-black mt-10 text-white w-48 rounded-3xl">
              Start Reading
            </button>
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default HeroBanner;
