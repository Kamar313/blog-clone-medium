import React from "react";
import Header from "./Home Page/Top Bar/Header";
import HeroBanner from "./Home Page/Hero Section/HeroBanner";
import TendingSection from "./Home Page/2nd Section/TendingSection";
import GloblePost from "./Home Page/Globle Post and Tags/GloblePost";

function Home() {
  return (
    <>
      <Header />
      <HeroBanner />
      <TendingSection />
      <GloblePost />
    </>
  );
}

export default Home;
