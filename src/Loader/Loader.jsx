import React from "react";
import "./loader.css";

function Loader() {
  return (
    <>
      <div className=" p-20">
        <div class="loader"></div>
      </div>
    </>
  );
}

export default Loader;
