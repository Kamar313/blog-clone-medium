import React, { Component } from "react";
import Header from "../Home Page/Top Bar/Header";
import { connect } from "react-redux";
import { articaleTitle } from "../Redux/Action & Reducer /action";
import { articleSubTitle } from "../Redux/Action & Reducer /action";
import { articaleTag } from "../Redux/Action & Reducer /action";
import { articalebody } from "../Redux/Action & Reducer /action";
import { errorbody } from "../Redux/Action & Reducer /action";
import { errorSubTitle } from "../Redux/Action & Reducer /action";
import { errorTitle } from "../Redux/Action & Reducer /action";

class NewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validation: true,
    };
  }
  fetchApi = (data) => {
    let athorize = JSON.parse(localStorage.getItem("data"));
    fetch("https://api.realworld.io/api/articles", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        authorization: `Token ${athorize.user.token}`,
      },
      body: JSON.stringify(data),
    })
      .then((ele) => ele.json())
      .then((data) => console.log(data));
  };
  acrticaleValidation = () => {
    let title = this.props.data.data.articaleTitle;
    let subTitle = this.props.data.data.articalesubTitle;
    let body = this.props.data.data.articalebody;
    let tag = this.props.data.data.articalTags;
    if (title === "") {
      this.props.errorTitle("Title should not be empty.");
      this.setState({ validation: true });
      console.log(this.state.validation);
    }
    if (subTitle == "") {
      this.props.errorSubTitle("Subtitle should not be empty.");
      this.setState({ validation: true });
    }
    if (body == "") {
      this.props.errorbody("Body should not be empty.");
      this.setState({ validation: true });
    }
    let tagList = tag.split(",").map((tag) => tag.trim());
    let data = {
      article: {
        title: title,
        description: subTitle,
        body: body,
        tagList: tagList,
      },
    };
    if (!this.state.validation) {
      this.fetchApi(data);
    }
  };
  checkError = () => {
    let title = this.props.data.data.articaleTitle;
    let subTitle = this.props.data.data.articalesubTitle;
    let body = this.props.data.data.articalebody;
    let bodyError = this.props.data.data.errorarticalebody;
    let subtitleError = this.props.data.data.errorarticalesubTitle;
    let titleerror = this.props.data.data.errorarticaleTitle;
    if (
      title != "" &&
      subTitle != "" &&
      body != "" &&
      bodyError == "" &&
      subtitleError == "" &&
      titleerror == ""
    ) {
      this.setState({ validation: false });
    }
  };
  onChangeArticaleTitle = (e) => {
    this.props.articaleTitle(e.target.value);
    if (e.target.value == "") {
      this.props.errorTitle("Title should not be empty.");
      this.setState({ validation: true });
    } else if (e.target.value.length < 6) {
      this.props.errorTitle("Title is to Short.");
      this.setState({ validation: true });
    } else {
      this.props.errorTitle("");
    }
    this.checkError();
  };
  onChangeArticaleSubTitle = (e) => {
    this.props.articleSubTitle(e.target.value);
    if (e.target.value == "") {
      this.props.errorSubTitle("Subtitle should not be empty.");
      this.setState({ validation: true });
    } else {
      this.props.errorSubTitle("");
    }
    this.checkError();
  };
  onChangeArticaleBody = (e) => {
    this.props.articalebody(e.target.value);
    if (e.target.value == "") {
      this.props.errorbody("Body should not be empty.");
      this.setState({ validation: true });
    } else if (e.target.value.length < 6) {
      this.props.errorbody("Body is to Short.");
      this.setState({ validation: true });
    } else {
      this.props.errorbody("");
    }
    this.checkError();
  };
  onChangeArticaleTags = (e) => {
    this.props.articaleTag(e.target.value);
  };
  render() {
    return (
      <>
        <Header />
        <div className=" pt-20 flex justify-center items-center flex-col">
          <div className=" w-4/6 mt-6 ">
            <input
              className="placeholder:italic  placeholder:text-xl h-14  max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Article Title"
              onChange={(e) => this.onChangeArticaleTitle(e)}
              type="text"
              name="search"
            />
            <span className=" text-red-500">
              {this.props.data.data.errorarticaleTitle}
            </span>
            <input
              className="placeholder:italic h-12 mt-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Whats This Article About?"
              onChange={(e) => this.onChangeArticaleSubTitle(e)}
              type="text"
              name="search"
            />
            <span className=" text-red-500">
              {this.props.data.data.errorarticalesubTitle}
            </span>
            <textarea
              name="for Article"
              onChange={(e) => this.onChangeArticaleBody(e)}
              className="placeholder:italic mt-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              cols="30"
              rows="8"
            ></textarea>
            <span className=" text-red-500">
              {this.props.data.data.errorarticalebody}
            </span>
            <input
              className="placeholder:italic h-10 mt-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Whats This Articale About?"
              onChange={(e) => this.onChangeArticaleTags(e)}
              type="text"
              name="search"
            />
          </div>
          <button
            onClick={this.acrticaleValidation}
            className=" h-10 w-36 bg-green-700 text-xl shadow-lg rounded-lg mt-4"
          >
            Publish Article
          </button>
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return { data: state };
}
export default connect(mapStateToProps, {
  articaleTitle,
  articleSubTitle,
  articaleTag,
  articalebody,
  errorTitle,
  errorbody,
  errorSubTitle,
})(NewPost);
