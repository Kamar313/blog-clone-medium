import React, { Component } from "react";
import Header from "../Home Page/Top Bar/Header";
import { NavLink } from "react-router-dom";

export default class Profile extends Component {
  render() {
    let objData = JSON.parse(localStorage.getItem("data"));
    return (
      <>
        <Header />
        <div className=" w-6/6 h-full pt-24">
          <div className=" w-6/6 bg-slate-100 flex flex-col items-center  h-56 justify-center l">
            <img
              src="./photos/profile-picture-973460_1280.webp"
              alt="Profile-photo"
              className=" w-24 h-24 rounded-full mb-4"
            ></img>
            <h2 className=" text-xl mb-4">
              {objData.user.username.toUpperCase()}
            </h2>

            <button className=" self-end mr-20 border-2 bg-[#5cb85c]  border-black shadow-md rounded px-2">
              <NavLink to="/settings"> Edit Profile </NavLink>
            </button>
          </div>
        </div>
      </>
    );
  }
}
