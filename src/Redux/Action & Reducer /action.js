// Data Feching
export let dataFetch = () => (dispatch) => {
  fetch(`https://api.realworld.io/api/articles`)
    .then((data) => data.json())
    .then((data) => dispatch({ type: "DATA_FECHED", payload: data.articles }));
};
export let tagFetch = () => (dispatch) => {
  fetch(`https://api.realworld.io/api/tags`)
    .then((data) => data.json())
    .then((data) => dispatch({ type: "TAGS_FECHED", payload: data }));
};

// Post Requst for SignUP
export let postRequsetSignUp = (data) => (dispatch) => {
  fetch(`https://api.realworld.io/api/users`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((res) => {
      if (!res.ok) {
        res
          .json()
          .then((error) =>
            dispatch({ type: "POST_RESPONCE_ERROR", payload: error })
          );
      }
      return res.json();
    })
    .then((data) => {
      dispatch({ type: "POST_RESPONCE_SUCSSES", payload: data });
      dispatch({ type: "KEY_USER_LOGIN", payload: JSON.stringify(data) });
    });
};

// Post Requst For LogIN
export let postRequsetlogin = (data) => (dispatch) => {
  fetch(`https://api.realworld.io/api/users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((res) => {
      if (!res.ok) {
        res
          .json()
          .then((error) =>
            dispatch({ type: "POST_RESPONCE_ERROR_LOGIN", payload: error })
          );
      }
      return res.json();
    })
    .then((data) => {
      dispatch({ type: "POST_RESPONCE_LOGIN", payload: data });
      dispatch({ type: "KEY_USER_LOGIN", payload: JSON.stringify(data) });
    });
};
// Validation Login
export let errorEmail = (data) => {
  return { type: "EMAIL_Error", payload: data };
};
export let findError = (data) => {
  return { type: "FIND_ERROR", payload: data };
};
export let passError = (data) => {
  return { type: "PASS_Error", payload: data };
};
export let inputValue = (data) => {
  return { type: "INPUT_VALUE", payload: data };
};
export let inputPassfun = (data) => {
  return { type: "inputPass", payload: data };
};

// Validation Signup
export let signupEmail = (payload) => {
  return { type: "SIGNUP_EMAIL", payload: payload };
};
export let signupPass = (payload) => {
  return { type: "SIGNUP_PASS", payload: payload };
};
export let signupUserName = (payload) => {
  return { type: "SIGNUP_USER", payload: payload };
};
export let errorSignupEmail = (payload) => {
  return { type: "SIGNUP_EMAIL_ERROR", payload: payload };
};
export let errorSignupPass = (payload) => {
  return { type: "SIGNUP_PASS_ERROR", payload: payload };
};
export let errorSignupUserName = (payload) => {
  return { type: "SIGNUP_USER_ERROR", payload: payload };
};
export let filterbyTag = (payload) => {
  return { type: "FILTERED_BY_TAGS", payload: payload };
};
export let articaleTitle = (payload) => {
  return { type: "ARTICAL_TITLE", payload: payload };
};
export let articleSubTitle = (payload) => {
  return { type: "ARTICAL_SUBTITLE", payload: payload };
};
export let articalebody = (payload) => {
  return { type: "ARTICAL_BODY", payload: payload };
};
export let articaleTag = (payload) => {
  return { type: "ARTICAL_TAG", payload: payload };
};

export let errorTitle = (payload) => {
  return { type: "Error_articalTitale", payload: payload };
};
export let errorSubTitle = (payload) => {
  return { type: "Error_articalSubTitale", payload: payload };
};
export let errorbody = (payload) => {
  return { type: "Error_articlbody", payload: payload };
};
