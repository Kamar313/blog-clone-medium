import { combineReducers } from "redux";

const initial = {
  loading: true,
  data: null,
  tags: "",
  emailError: "",
  errorpassWord: "",
  inputEmail: "",
  inputPass: "",
  findError: true,
  signupEmail: "",
  SignupUserName: "",
  SignupPassword: "",
  errorSignUp: "",
  errorUserName: "",
  errorPassword: "",
  postRequstError: null,
  postResponceSucss: null,
  loginPostRequst: null,
  loginpostError: null,
  localKey: null,
  articaleTitle: "",
  articalesubTitle: "",
  articalebody: "",
  articalTags: "",
  errorarticaleTitle: "",
  errorarticalesubTitle: "",
  errorarticalebody: "",
  filteredData: "",
};

const data = (state = initial, action) => {
  switch (action.type) {
    case "DATA_FECHED":
      return { ...state, loading: false, data: action.payload };
    case "TAGS_FECHED":
      return { ...state, tags: action.payload };
    case "EMAIL_Error":
      return { ...state, emailError: action.payload };
    case "PASS_Error":
      return { ...state, errorpassWord: action.payload };
    case "INPUT_VALUE":
      return { ...state, inputEmail: action.payload };
    case "inputPass":
      return { ...state, inputPass: action.payload };
    case "FIND_ERROR":
      return { ...state, findError: action.payload };
    case "SIGNUP_EMAIL":
      return { ...state, signupEmail: action.payload };
    case "SIGNUP_PASS":
      return { ...state, SignupPassword: action.payload };
    case "SIGNUP_USER":
      return { ...state, SignupUserName: action.payload };
    case "SIGNUP_EMAIL_ERROR":
      return { ...state, errorSignUp: action.payload };
    case "SIGNUP_PASS_ERROR":
      return { ...state, errorPassword: action.payload };
    case "SIGNUP_USER_ERROR":
      return { ...state, errorUserName: action.payload };
    case "POST_RESPONCE_SUCSSES":
      return { ...state, postResponceSucss: action.payload };
    case "POST_RESPONCE_ERROR":
      return { ...state, postRequstError: action.payload };
    case "POST_RESPONCE_LOGIN":
      return { ...state, loginPostRequst: action.payload };
    case "POST_RESPONCE_ERROR_LOGIN":
      return { ...state, loginpostError: action.payload };
    case "KEY_USER_LOGIN":
      return { ...state, localKey: action.payload };
    case "FILTERED_BY_TAGS":
      let filterTag = state.data.reduce((acc, curr) => {
        if (curr.tagList.includes(action.payload)) {
          acc.push(curr);
        }
        return acc;
      }, []);
      return { ...state, filteredData: filterTag };

    case "ARTICAL_TITLE":
      return { ...state, articaleTitle: action.payload };
    case "ARTICAL_SUBTITLE":
      return { ...state, articalesubTitle: action.payload };
    case "ARTICAL_BODY":
      return { ...state, articalebody: action.payload };
    case "ARTICAL_TAG":
      return { ...state, articalTags: action.payload };
    case "Error_articalTitale":
      return { ...state, errorarticaleTitle: action.payload };
    case "Error_articalSubTitale":
      return { ...state, errorarticalesubTitle: action.payload };
    case "Error_articlbody":
      return { ...state, errorarticalebody: action.payload };
    default:
      return { ...state };
  }
};
export default combineReducers({ data });
