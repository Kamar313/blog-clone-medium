import { legacy_createStore, compose, applyMiddleware } from "redux";
import reducer from "./Action & Reducer /reducer";
import thunk from "redux-thunk";
const store = legacy_createStore(reducer, compose(applyMiddleware(thunk)));
export default store;
