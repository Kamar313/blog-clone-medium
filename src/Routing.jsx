import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Home";
import SignIn from "./SignIn/SignIn";
import SignUp from "./Signup/SignUp";
import NewPost from "./New Post/NewPost";
import Profile from "./Profile/Profile";
import SingleArticle from "./Home Page/Globle Post and Tags/SingleArticle";
import Setting from "./Your Settings/Setting";
import Error from "./Error Page/Error";
function Routing() {
  let objData = JSON.parse(localStorage.getItem("data"));
  return (
    <BrowserRouter>
      {objData ? (
        <>
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/signin" exact>
              <SignIn />
            </Route>
            <Route path="/signup" exact>
              <SignUp />
            </Route>
            <Route path="/newPost" exact>
              <NewPost />
            </Route>
            <Route path="/profile" exact>
              <Profile />
            </Route>
            <Route path="/articles/:slug" component={SingleArticle}></Route>
            <Route path="/settings" exact>
              <Setting />
            </Route>
            <Route to="*" exact>
              <Error />
            </Route>
          </Switch>
        </>
      ) : (
        <>
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/signin" exact>
              <SignIn />
            </Route>
            <Route path="/signup" exact>
              <SignUp />
            </Route>
            <Route path="/newPost" exact>
              <SignIn />
            </Route>
            <Route path="/profile" exact>
              <SignIn />
            </Route>
            <Route path="/articles/:slug" component={SingleArticle}></Route>
            <Route path="/settings" exact>
              <SignIn />
            </Route>
            <Route to="*" exact>
              <Error />
            </Route>
          </Switch>
        </>
      )}
    </BrowserRouter>
  );
}

export default Routing;
