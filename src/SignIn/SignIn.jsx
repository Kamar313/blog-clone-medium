import React, { Component } from "react";
import Header from "../Home Page/Top Bar/Header";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import {
  errorEmail,
  findError,
  passError,
  inputValue,
  inputPassfun,
  postRequsetlogin,
} from "../Redux/Action & Reducer /action";

class SignIn extends Component {
  checkError = () => {
    let email = this.props.data.data.inputEmail;
    let pass = this.props.data.data.inputPass;
    let emailError = this.props.data.data.emailError;
    let passwordError = this.props.data.data.errorpassWord;
    if (passwordError == "" && emailError == "" && pass != "" && email != "") {
      this.props.findError(false);
    }
  };

  emaiError = () => {
    let email = this.props.data.data.inputEmail;
    let pass = this.props.data.data.inputPass;
    if (email == "") {
      this.props.errorEmail("Email should not be empty.");
      this.props.findError(true);
    }
    if (pass == "") {
      this.props.passError("Password should not be empty.");
      this.props.findError(true);
    }
    if (!this.props.data.data.findError) {
      let obj = {
        user: {
          email: email,
          password: pass,
        },
      };
      this.props.postRequsetlogin(obj);
      if (this.props.data.data.loginPostRequst) {
        localStorage.setItem("data", this.props.data.data.localKey);
        if (localStorage.getItem("data") != "") {
          window.location.replace("/");
        }
      }
    }
  };
  changeHandler = (e) => {
    this.props.inputValue(e.target.value);
    let value = e.target;
    let notinclud = !value.value.includes("@");
    this.checkError();
    switch (true) {
      case value.value == "":
        return (
          this.props.errorEmail("Email should not be empty"),
          this.props.findError(true)
        );
      case notinclud:
        return (
          this.props.errorEmail("Please provide the correct Email"),
          this.props.findError(true)
        );

      default:
        return this.props.errorEmail("");
    }
  };
  passHandler = (e) => {
    this.props.inputPassfun(e.target.value);
    let value = e.target;
    this.checkError();
    switch (true) {
      case value.value == "":
        return (
          this.props.passError("Password should not be empty."),
          this.props.findError(true)
        );
      case value.value.length < 6:
        return (
          this.props.passError("Please provide the correct Password"),
          this.props.findError(true)
        );
      default:
        return this.props.passError("");
    }
  };
  render() {
    console.log(this.props.data.data);
    return (
      <>
        <Header />
        <div className=" w-1/1 flex justify-center">
          <div className="  w-3/6 bg-slate-100 h-96 mt-24 max-sm:w-5/6 flex flex-col justify-center items-center rounded-xl shadow-lg">
            <h2 className=" text-4xl mb-10 font-medium">Log In</h2>
            <span className=" text-red-500">
              {this.props.data.data.loginpostError
                ? `Email and Password  ${this.props.data.data.loginpostError.errors["email or password"][0]}`
                : ""}
            </span>
            <input
              className="placeholder:italic max-sm:w-5/6 placeholder:text-slate-400 block bg-white w-3/6 border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Enter Your Mail ID"
              onChange={(e) => this.changeHandler(e)}
              type="text"
              name="Email"
            />
            <span className=" mb-10 text-red-500">
              {this.props.data.data.emailError}
            </span>
            <input
              className="placeholder:italic max-sm:w-5/6 placeholder:text-slate-400 block bg-white w-3/6 border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Enter Your Password"
              onChange={(e) => this.passHandler(e)}
              type="password"
              name="search"
            />
            <span className=" mb-10 text-red-500">
              {this.props.data.data.errorpassWord}
            </span>
            <button
              onClick={this.emaiError}
              className=" hover:text-black hover:bg-yellow-400     bg-black mb-4 text-white w-28 h-10 rounded-xl"
            >
              Log In
            </button>
            <h4>
              Don't have Account?
              <NavLink to="/signup">
                <span className=" hover:text-yellow-400 cursor-pointer underline font-medium">
                  Create Account
                </span>
              </NavLink>
            </h4>
          </div>
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return { data: state };
}
export default connect(mapStateToProps, {
  errorEmail,
  findError,
  passError,
  inputValue,
  inputPassfun,
  postRequsetlogin,
})(SignIn);
