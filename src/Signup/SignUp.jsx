import React, { Component } from "react";
import Header from "../Home Page/Top Bar/Header";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import {
  signupEmail,
  signupPass,
  signupUserName,
  errorSignupEmail,
  errorSignupPass,
  errorSignupUserName,
  findError,
  postRequsetSignUp,
} from "../Redux/Action & Reducer /action";

class SignUp extends Component {
  checkError = () => {
    let email = this.props.data.data.signupEmail;
    let pass = this.props.data.data.SignupPassword;
    let checkerror = this.props.data.data.errorSignUp;
    let userName = this.props.data.data.SignupUserName;
    let userEmail = this.props.data.data.errorSignUp;
    let userPass = this.props.data.data.errorPassword;
    if (
      checkerror == "" &&
      userEmail == "" &&
      userPass == "" &&
      email != "" &&
      pass != "" &&
      userName != ""
    ) {
      this.props.findError(false);
      console.log(this.props.data.data.findError);
    }
  };
  // OnSubmit Error Handel
  onClicksubmit = () => {
    let email = this.props.data.data.signupEmail;
    let pass = this.props.data.data.SignupPassword;
    let userName = this.props.data.data.SignupUserName;

    if (email == "") {
      this.props.errorSignupEmail("Email should not be empty.");
      this.props.findError(true);
    }
    if (pass == "") {
      this.props.errorSignupPass("Password should not be empty.");
      this.props.findError(true);
    }
    if (userName == "") {
      this.props.errorSignupUserName("Name should not be empty.");
      this.props.findError(true);
    }
    if (!this.props.data.data.findError) {
      let obj = {
        user: {
          email: email,
          password: pass,
          username: userName,
        },
      };
      this.props.postRequsetSignUp(obj);
      if (this.props.data.data.postResponceSucss) {
        localStorage.setItem("data", this.props.data.data.localKey);
        if (localStorage.getItem("data") != "") {
          window.location.replace("/");
        }
      }
    }
    this.checkError();
  };
  //   OnChange Error Handel
  onChangeEmail = (e) => {
    this.props.signupEmail(e.target.value);
    let value = e.target;
    let notinclud = !value.value.includes("@");
    this.checkError();
    switch (true) {
      case value.value == "":
        return (
          this.props.errorSignupEmail("Email should Not be empty."),
          this.props.findError(true)
        );
      case notinclud:
        return (
          this.props.errorSignupEmail("Please provide the correct Email ID"),
          this.props.findError(true)
        );
      default:
        return this.props.errorSignupEmail("");
    }
  };
  onChangePass = (e) => {
    this.props.signupPass(e.target.value);
    let value = e.target;
    let checkerror = this.props.data.data.errorPassword;
    this.checkError();
    switch (true) {
      case value.value == "":
        return (
          this.props.errorSignupPass("Password should not be empty."),
          this.props.findError(true)
        );
      case value.value.length < 6:
        return (
          this.props.errorSignupPass("Please provide the correct Password"),
          this.props.findError(true)
        );
      default:
        return this.props.errorSignupPass("");
    }
  };
  onChangeUser = (e) => {
    this.props.signupUserName(e.target.value);
    let value = e.target;
    this.checkError();
    switch (true) {
      case value.value == "":
        return (
          this.props.errorSignupUserName("Name should not be empty."),
          this.props.findError(true)
        );
      case value.value.length < 6:
        return (
          this.props.errorSignupUserName("Please provide the correct Name"),
          this.props.findError(true)
        );
      default:
        return this.props.errorSignupUserName("");
    }
  };

  render() {
    console.log(this.props.data.data);
    return (
      <>
        <Header />
        <div className=" w-1/1 flex justify-center">
          <div className=" w-3/6 bg-slate-100 h-[450px] mt-24 max-sm:mt-16 max-sm:w-5/6 flex flex-col justify-center items-center rounded-xl shadow-lg">
            <h2 className=" text-4xl mb-10 font-medium">Sign Up</h2>
            <span className="text-red-500">
              {this.props.data.data.postRequstError == null
                ? ""
                : `Email ID ${this.props.data.data.postRequstError.errors.email[0]}` ||
                  `Username ${this.props.data.data.postRequstError.errors.username[0]}`}
            </span>
            <input
              className="placeholder:italic placeholder:text-slate-400 max-sm:w-5/6 block bg-white w-3/6 border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Enter Your Name"
              onChange={(e) => this.onChangeUser(e)}
              type="text"
              name="search"
            />
            <span className=" mb-10 text-red-500">
              {this.props.data.data.errorUserName}
            </span>
            <input
              className="placeholder:italic placeholder:text-slate-400 max-sm:w-5/6 block bg-white w-3/6 border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Enter Your Mail ID"
              onChange={(e) => this.onChangeEmail(e)}
              type="text"
              name="search"
            />
            <span className=" mb-10 text-red-500">
              {this.props.data.data.errorSignUp}
            </span>
            <input
              className="placeholder:italic placeholder:text-slate-400 max-sm:w-5/6 block bg-white w-3/6 border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Enter Your Password"
              onChange={(e) => this.onChangePass(e)}
              type="password"
              name="search"
            />
            <span className=" mb-10 text-red-500">
              {this.props.data.data.errorPassword}
            </span>
            <button
              onClick={this.onClicksubmit}
              className=" hover:text-black hover:bg-yellow-400 bg-black mb-4 text-white w-28 h-10 rounded-xl"
            >
              Sign Up
            </button>
            <h4>
              You have Account?
              <NavLink to="/signin">
                <span className=" hover:text-yellow-400 cursor-pointer underline font-medium">
                  Log In
                </span>
              </NavLink>
            </h4>
          </div>
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return { data: state };
}
export default connect(mapStateToProps, {
  signupEmail,
  signupPass,
  signupUserName,
  errorSignupEmail,
  errorSignupPass,
  errorSignupUserName,
  findError,
  postRequsetSignUp,
})(SignUp);
