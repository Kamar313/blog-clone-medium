import React, { Component } from "react";
import Header from "../Home Page/Top Bar/Header";

class Setting extends Component {
  render() {
    let objData = JSON.parse(localStorage.getItem("data"));
    console.log(objData);
    return (
      <>
        <Header />
        <div className=" pt-20 text-center flex items-center flex-col mb-4">
          <div className=" w-4/6">
            <h1 className=" text-4xl mt-2 mb-4">Your Settings</h1>
            <input
              className="placeholder:italic placeholder:text-xl h-10 mb-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="URL of Profile Picture"
              value={objData.user.image}
              type="text"
              name="search"
            />
            <input
              className="placeholder:italic placeholder:text-xl h-8 mb-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Username"
              type="text"
              name="search"
              value={objData.user.username}
            />
            <textarea
              name="for Articlee"
              placeholder="Tell me about Yourself"
              className="placeholder:italic placeholder:text-2xl max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              cols="30"
              rows="8"
            ></textarea>
            <input
              className="placeholder:italic placeholder:text-xl h-10 mt-4 mb-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Your Email ID"
              type="text"
              name="search"
              value={objData.user.email}
            />
            <input
              className="placeholder:italic placeholder:text-xl h-8 mb-4 max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Update Password"
              type="text"
              name="search"
            />
          </div>
          <button className=" bg-green-500 p-3 rounded-lg">
            Update Profile
          </button>
        </div>
      </>
    );
  }
}
export default Setting;
