import React from "react";
import ReactDOM from "react-dom";
import Routing from "./Routing";
import store from "./Redux/store";
import { Provider } from "react-redux";
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Routing />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
